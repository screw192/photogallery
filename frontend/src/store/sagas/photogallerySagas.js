import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
  fetchPhotosRequest,
  fetchPhotosSuccess,
  fetchPhotosFailure, removePhotoRequest, addPhotoRequest,
} from "../actions/photogalleryActions";
import {addNotification} from "../actions/notifierActions";
import {historyPush} from "../actions/historyActions";

export function* fetchPhotos({payload: userID}) {
  try {
    let response;
    if (!userID) {
      response = yield axiosApi.get("/photos");
    } else {
      response = yield axiosApi.get(`/photos/${userID}`);
    }

    yield put(fetchPhotosSuccess(response.data));
  } catch (e) {
    yield put(fetchPhotosFailure(e.response.data));
    yield put(addNotification({message: "Failed to fetch photos", options: {variant: "error"}}));
  }
}

export function* addPhoto({payload: data}) {
  try {
    yield axiosApi.post("/photos", data.photoData);

    yield put(historyPush(`/users/${data.currentUserID}`));
    yield put(addNotification({message: "Photo successfully uploaded", options: {variant: "success"}}))
  } catch (e) {
    yield put(addNotification({message: "Failed to upload photo", options: {variant: "error"}}));
  }
}

export function* removePhoto({payload: data}) {
  try {
    const response =  yield axiosApi.delete(`/photos/${data.photoID}`);

    yield put(historyPush(`/users/${data.currentUserID}`));
    yield put(addNotification({message: response.data.message, options: {variant: "success"}}))
  } catch (e) {
    yield put(addNotification({message: "Failed to remove photo", options: {variant: "error"}}));
  }
}

const photogallerySagas = [
    takeEvery(fetchPhotosRequest, fetchPhotos),
    takeEvery(removePhotoRequest, removePhoto),
    takeEvery(addPhotoRequest, addPhoto),
];

export default photogallerySagas;