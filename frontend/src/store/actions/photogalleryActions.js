import photogallerySlice from "../slices/photogallerySlice";

export const {
  fetchPhotosRequest,
  fetchPhotosSuccess,
  fetchPhotosFailure,
    addPhotoRequest,
  removePhotoRequest,
} = photogallerySlice.actions;