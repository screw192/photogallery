import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  photos: [],
  author: null,
  photosLoading: true,
  photosError: null,
  uploadPhotoLoading: false,
  uploadPhotoError: null,
};

const name = "photogallery";

const photogallerySlice = createSlice({
  name,
  initialState,
  reducers: {
    fetchPhotosRequest: state => {
      state.photosLoading = true;
    },
    fetchPhotosSuccess: (state, {payload: photosData}) => {
      state.photos = photosData.photos;
      state.author = photosData.author;
      state.photosLoading = false;
    },
    fetchPhotosFailure: (state, {payload: error}) => {
      state.photosLoading = false;
      state.photosError = error;
    },
    addPhotoRequest: () => {},
    removePhotoRequest: () => {},
  }
});

export default photogallerySlice;