import {combineReducers} from "redux";

import usersSlice from "./slices/usersSlice";
import notifierSlice from "./slices/notifierSlice";
import photogallerySlice from "./slices/photogallerySlice";


const rootReducer = combineReducers({
  users: usersSlice.reducer,
  notifier: notifierSlice.reducer,
  photogallery: photogallerySlice.reducer,
});

export default rootReducer;