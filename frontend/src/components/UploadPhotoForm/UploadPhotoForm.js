import React, {useState} from 'react';
import {useSelector} from "react-redux";
import {Grid} from "@material-ui/core";

import FormElement from "../UI/Form/FormElement";
import FileInput from "../UI/Form/FileInput";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";


const UploadPhotoForm = ({onSubmit, error, loading}) => {
  const currentUserID = useSelector(state => state.users.user?._id);
  const [newPhoto, setNewPhoto] = useState({
    title: "",
    photo: "",
  });

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(newPhoto).forEach(key => {
      formData.append(key, newPhoto[key]);
    });

    onSubmit({photoData: formData, currentUserID});
  };

  const photoInputChangeHandler = e => {
    const value = e.target.value;

    setNewPhoto(prevState => ({
      ...prevState,
      title: value
    }));
  };

  const photoImageChangeHandler = e => {
    const file = e.target.files[0];

    setNewPhoto(prevState => ({
      ...prevState,
      photo: file
    }));
  };


  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  return (
      <Grid
          container
          spacing={1}
          direction="column"
          component="form"
          onSubmit={submitFormHandler}
          noValidate
      >
        <FormElement
            required
            label="Title"
            type="text"
            onChange={photoInputChangeHandler}
            value={newPhoto.title}
            error={getFieldError("title")}
        />
        <Grid item xs>
          <FileInput
              label="Photo"
              onChange={photoImageChangeHandler}
              error={getFieldError("photo")}
          />
        </Grid>
        <Grid item>
          <ButtonWithProgress
              type="submit"
              variant="contained"
              color="primary"
              loading={loading}
              disabled={loading}
          >
            Upload photo
          </ButtonWithProgress>
        </Grid>
      </Grid>
  );
};

export default UploadPhotoForm;