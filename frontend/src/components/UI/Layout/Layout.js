import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import {Box, Container} from "@material-ui/core";

import AppToolbar from "../AppToolbar/AppToolbar";
import Notifier from "../../../containers/Notifier/Notifier";


const Layout = ({children}) => {
  return (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Box mb={10} component={Container} maxWidth="lg">
          {children}
        </Box>
      </main>
      <Notifier/>
    </>
  );
};

export default Layout;