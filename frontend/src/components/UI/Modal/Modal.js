import React from 'react';
import {useSelector} from "react-redux";
import {IconButton, makeStyles, Modal} from "@material-ui/core";
import CancelIcon from '@material-ui/icons/Cancel';

import {apiURL} from "../../../config";


const useStyles = makeStyles({
  paper: {
    position: "absolute",
    outline: "none",
    backgroundColor: "#fff",
    borderRadius: "13px",
    padding: "13px",
  },
  image: {
    borderRadius: "3px",
    display: "block"
  },
  closeModalButton: {
    position: "absolute",
    top: 0,
    right: "-35px",
    backgroundColor: "#ffffff",
    opacity: 0.4,
    padding: "3px",
    "&:hover": {
      opacity: 1,
      backgroundColor: "#ffffff",
    }
  }
});

const SimpleModal = ({isOpen, i, handleClose}) => {
  const classes = useStyles();
  const modalStyle = {
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  };
  const photosData = useSelector(state => state.photogallery.photos);

  const body = (
      <div style={modalStyle} className={classes.paper}>
        <img src={apiURL + "/" + photosData[i]?.photo} alt=""/>
        <IconButton
            color="default"
            className={classes.closeModalButton}
            onClick={handleClose}
        >
          <CancelIcon />
        </IconButton>
      </div>
  );

  return (
      <div>
        <Modal open={isOpen}>
          {body}
        </Modal>
      </div>
  );
}

export default SimpleModal;