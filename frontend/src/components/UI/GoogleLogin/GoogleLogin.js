import React from "react";
import {useDispatch} from "react-redux";
import GoogleLoginButton from "react-google-login";
import {Button} from "@material-ui/core";

import {googleClientId} from "../../../config";
import {googleLoginRequest} from "../../../store/actions/usersActions";
import GoogleIcon from "../GoogleIcon/GoogleIcon";


const GoogleLogin = () => {
  const dispatch = useDispatch();

  const handleLogin = response => {
    dispatch(googleLoginRequest(response));
  };

  return (
    <GoogleLoginButton
      clientId={googleClientId}
      render={props => (
        <Button
          fullWidth
          color="primary"
          variant="outlined"
          startIcon={<GoogleIcon/>}
          onClick={props.onClick}
        >
          Login with Google
        </Button>
      )}
      onSuccess={handleLogin}
      cookiePolicy={"single_host_origin"}
    />
  );
};

export default GoogleLogin;