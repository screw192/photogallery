import React from "react";
import {Route, Switch} from "react-router-dom";
import {Helmet} from "react-helmet";

import Layout from "./components/UI/Layout/Layout";
import Photogallery from "./containers/Photogallery/Photogallery";
import AddPhoto from "./containers/AddPhoto/AddPhoto";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";


const App = () => {
  return (
    <Layout>
      <Helmet
        titleTemplate="Photogallery - %s"
        defaultTitle="Photogallery"
      />
      <Switch>
        <Route path="/" exact component={Photogallery}/>
        <Route path="/add_photo" exact component={AddPhoto}/>
        <Route path="/users/:id" exact component={Photogallery}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
      </Switch>
    </Layout>
  );
};

export default App;
