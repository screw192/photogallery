import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {Card, CardContent, CardMedia, Grid, IconButton, makeStyles, Typography} from "@material-ui/core";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

import {apiURL} from "../../config";
import {removePhotoRequest} from "../../store/actions/photogalleryActions";


const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    position: "relative",
  },
  media: {
    height: 140,
  },
  author: {
    fontWeight: "500",
  },
  authorLink: {
    textDecoration: "none",
    color: "#3f51b5",
  },
  removeButton: {
    padding: "2px",
    backgroundColor: "#ffffff99",
    position: "absolute",
    top: "5px",
    right: "5px",
    "&:hover": {
      backgroundColor: "#fff",
    }
  }
});

const PhotogalleryItem = ({authorID, photoID, photo, title, name, handleModalOpen, index}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const currentUserID = useSelector(state => state.users.user?._id);
  const photosAuthor = useSelector(state => state.photogallery.author);

  const removePhotoHandler = event => {
    event.preventDefault();
    dispatch(removePhotoRequest({photoID, currentUserID}));
  };

  return (
      <Grid item xs={6} sm={4} md={3}>
        <Card className={classes.root}>
          <CardMedia
              className={classes.media}
              image={`${apiURL}/${photo}`}
              title={title}
              onClick={() => handleModalOpen(index)}
          />
          <CardContent>
            <Typography variant="h6" color="textSecondary" noWrap>
              {title}
            </Typography>
            {!photosAuthor &&
              <Typography className={classes.author} variant="body1" noWrap>
                By: <Link to={`/users/${authorID}`} className={classes.authorLink}>{name}</Link>
              </Typography>
            }
          </CardContent>
          {photosAuthor && currentUserID === authorID &&
            <IconButton className={classes.removeButton} color="secondary" onClick={e => removePhotoHandler(e)}>
              <DeleteForeverIcon/>
            </IconButton>
          }
        </Card>
      </Grid>
  );
};

export default PhotogalleryItem;