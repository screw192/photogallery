import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Button, Grid, Typography} from "@material-ui/core";
import AddCircleIcon from '@material-ui/icons/AddCircle';

import {fetchPhotosRequest} from "../../store/actions/photogalleryActions";
import PhotogalleryItem from "./PhotogalleryItem";
import Preloader from "../../components/UI/Preloader/Preloader";
import SimpleModal from "../../components/UI/Modal/Modal";
import {Link} from "react-router-dom";


const modalInitialState = {
  open: false,
  targetIndex: "",
}

const Photogallery = props => {
  const dispatch = useDispatch();
  const photosData = useSelector(state => state.photogallery.photos);
  const photosAuthor = useSelector(state => state.photogallery.author);
  const photosLoading = useSelector(state => state.photogallery.photosLoading);
  const currentUserID = useSelector(state => state.users.user?._id);
  const [modalState, setModalState] = useState(modalInitialState);

  const handleModalOpen = targetIndex => {
    setModalState(prevState => ({
      ...prevState,
      open: true,
      targetIndex
    }));
  };

  const handleModalClose = () => {
    setModalState({...modalInitialState});
  };

  useEffect(() => {
    if (props.match.params.id) {
      dispatch(fetchPhotosRequest(props.match.params.id));
    } else {
      dispatch(fetchPhotosRequest());
    }
  }, [dispatch, props]);

  const photoCards = photosData.map((photoItem, index) => {
    return (
        <PhotogalleryItem
            key={photoItem._id}
            photoID={photoItem._id}
            authorID={photoItem.user._id}
            index={index}
            photo={photoItem.photo}
            name={photoItem.user.displayName}
            title={photoItem.title}
            handleModalOpen={handleModalOpen}
        />
    );
  })

  return (
      <>
        {photosLoading ? (
            <Preloader/>
        ) : (
            <>
              <Grid container justify="space-between" alignItems="center">
                <Grid item xs>
                  {photosAuthor && <Typography variant="h6">{photosAuthor}'s gallery</Typography>}
                </Grid>
                <Grid item>
                  {currentUserID && currentUserID === props.match.params.id &&
                    <Button
                        variant="contained"
                        size="small"
                        color="primary"
                        endIcon={<AddCircleIcon/>}
                        component={Link}
                        to="/add_photo"
                    >
                      Add new photo
                    </Button>
                  }
                </Grid>
              </Grid>
              <Box pt={2} component={Grid} container spacing={4}>
                {photoCards}
              </Box>
              <SimpleModal
                  isOpen={modalState.open}
                  i={modalState.targetIndex}
                  handleClose={handleModalClose}
              />
            </>
        )}
      </>
  );
};

export default Photogallery;