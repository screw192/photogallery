import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Helmet} from "react-helmet";
import {Container, Typography} from "@material-ui/core";

import {addPhotoRequest} from "../../store/actions/photogalleryActions";
import UploadPhotoForm from "../../components/UploadPhotoForm/UploadPhotoForm";


const AddPhoto = () => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.photogallery.uploadPhotoLoading);
  const error = useSelector(state => state.photogallery.uploadPhotoError);

  const uploadPhotoFormSubmit = photoData => {
    dispatch(addPhotoRequest(photoData));
  };

  return (
      <Container maxWidth="md">
        <Helmet>
          <title>Add photo</title>
        </Helmet>
        <Typography variant="h5" gutterBottom>
          Add your photo
        </Typography>
        <UploadPhotoForm
            onSubmit={uploadPhotoFormSubmit}
            loading={loading}
            error={error}
        />
      </Container>
  );
};

export default AddPhoto;