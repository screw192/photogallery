const express = require("express");
const Photo = require("../models/Photo");
const User = require("../models/User");
const auth = require("../middleware/auth");
const upload = require("../multer").photo;

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const photoData = await Photo.find().populate("user", "displayName");

    res.send({photos: photoData, author: null});
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const author = await User.findOne({_id: req.params.id});
    const photoData = await Photo.find({user: req.params.id}).populate("user", "displayName");

    res.send({photos: photoData, author: author.displayName});
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, upload.single("photo"), async (req, res) => {
  try {
    const photoData = {
      user: req.user._id,
      title: req.body.title,
    };
    photoData.photo = req.file.filename;

    const photo = new Photo(photoData);
    await photo.save();

    res.send(photo);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete("/:id", auth, async (req, res) => {
  try {
    const photoToRemove = await Photo.findOne({_id: req.params.id});

    if (!photoToRemove.user.equals(req.user._id)) {
      return res.status(403).send({message: "You are allowed to remove only your own photos!"});
    }

    await Photo.deleteOne({_id: req.params.id});

    res.send({message: "Photo successfully deleted"});
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;