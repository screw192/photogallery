const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const Photo = require("./models/Photo");
const {nanoid} = require("nanoid");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) { 
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1, user2, user3, user4] = await User.create({
    email: "user1@test",
    password: "test",
    token: nanoid(),
    displayName: "Abu Makaka",
  }, {
    email: "user2@test",
    password: "test",
    token: nanoid(),
    displayName: "Yoba YellowKolobok",
    avatar: "fixtures/av01.png"
  }, {
    email: "user3@test",
    password: "test",
    token: nanoid(),
    displayName: "Anonim Legion"
  }, {
    email: "user4@test",
    password: "test",
    token: nanoid(),
    displayName: "Hikka-Tyan",
    avatar: "fixtures/av02.jpeg"
  });

  await Photo.create({
    title: "Benis Heheh",
    photo: "fixtures/01.jpg",
    user: user1
  }, {
    title: "Water TMNT",
    photo: "fixtures/02.jpg",
    user: user3
  }, {
    title: "Red fox",
    photo: "fixtures/03.jpg",
    user: user2
  }, {
    title: "Dusk tree",
    photo: "fixtures/04.jpg",
    user: user4
  }, {
    title: "Snow mountain",
    photo: "fixtures/05.jpg",
    user: user1
  }, {
    title: "Rocky beach",
    photo: "fixtures/06.jpg",
    user: user3
  }, {
    title: "Snowy road",
    photo: "fixtures/07.jpg",
    user: user2
  }, {
    title: "Mountain with clouds",
    photo: "fixtures/08.jpg",
    user: user2
  }, {
    title: "Sandy beach",
    photo: "fixtures/09.jpg",
    user: user4
  })

  await mongoose.connection.close();
};

run().catch(console.error);